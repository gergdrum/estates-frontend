import {Component, OnInit} from '@angular/core';
import {HttpClientService, User} from '../service/httpclient.service';
import {Router} from '@angular/router';
import {AuthenticationService} from '../service/authentication.service';

@Component({
  selector: 'app-add-users',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  roles = ['ADMIN', 'USER'];
  selectedRole: null;
  selectedUsername: null;
  selectedPassword: null;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private httpClientService: HttpClientService
  ) {
  }

  ngOnInit() {
  }

  createUser(): void {
    this.httpClientService.createUser(new User('', this.selectedPassword, this.selectedUsername, this.selectedRole))
      .subscribe(data => {
        if (this.authenticationService.isUserLoggedIn) {
          this.router.navigate(['']);
        } else {
          this.router.navigate(['login']);
        }
        alert('User created successfully.');
      });

  }
}
