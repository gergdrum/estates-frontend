import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';

export interface UserData {
  role: '';
  userName: '';
}


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  authenticate(username, password) {
    const user = {
      userName: username,
      password,
    };


    return this.httpClient.post('http://localhost:8080/users/login', user, {
      headers: {
        'Content-Type': 'application/json',
      },
    }).pipe(
      map(
        (userData: UserData) => {
          if (userData !== null) {
            sessionStorage.setItem('username', username);
            sessionStorage.setItem('role', userData.role);
          }
          return userData;
        }
      ));
  }

  isUserLoggedIn() {
    const user = sessionStorage.getItem('username');
    return !(user === null);
  }

  isUserAdmin() {
    if (sessionStorage.getItem('username') && sessionStorage.getItem('role') === 'ADMIN') {
      return true;
    }
  }

  logOut() {
    sessionStorage.removeItem('username');
  }
}
