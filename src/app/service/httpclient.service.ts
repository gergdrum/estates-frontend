import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

export class User {
  constructor(
    public id: string,
    public userName: string,
    public password: string,
    public role: string,
  ) {
  }
}

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(
    private httpClient: HttpClient
  ) {
  }


  getUsers() {
    return this.httpClient.get<User[]>('http://localhost:8080/users');
  }

  public deleteUser(user) {
    return this.httpClient.delete<User>('http://localhost:8080/users' + '/' + user.id);
  }

  public createUser(user) {
    return this.httpClient.post<User>('http://localhost:8080/users/create', user);
  }
}
