import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {LogoutComponent} from './logout/logout.component';
import {UserComponent} from './user/user.component';
import {AddUserComponent} from './add-user/add-user.component';
import {EstatesComponent} from './estates/estates.component';
import {AuthGuardService} from './service/auth-guard.service';

const routes: Routes = [
  {path: '', component: UserComponent, canActivate: [AuthGuardService], data: {expectedRole: 'ADMIN'}},
  {path: 'adduser', component: AddUserComponent, data: {expectedRole: 'ADMIN'}},
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent, canActivate: [AuthGuardService]},
  {path: 'estates', component: EstatesComponent, canActivate: [AuthGuardService]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
